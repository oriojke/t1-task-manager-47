package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;

public interface ITokenService {

    @Nullable String getToken();

    void setToken(@Nullable final String token);

}

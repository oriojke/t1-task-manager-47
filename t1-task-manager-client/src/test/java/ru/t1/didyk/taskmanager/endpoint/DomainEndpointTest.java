package ru.t1.didyk.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.api.endpoint.IDomainEndpoint;
import ru.t1.didyk.taskmanager.constant.SharedTestMethods;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;
import ru.t1.didyk.taskmanager.marker.IntegrationCategory;
import ru.t1.didyk.taskmanager.service.PropertyService;

@Category(IntegrationCategory.class)
public class DomainEndpointTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService);

    @Test
    public void domainBackupLoadTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataBackupLoadRequest request = new DataBackupLoadRequest(userToken);
        @Nullable DataBackupLoadResponse response = domainEndpoint.domainBackupLoad(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainBackupSaveTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataBackupSaveRequest request = new DataBackupSaveRequest(userToken);
        @Nullable DataBackupSaveResponse response = domainEndpoint.domainBackupSave(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainBase64LoadTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataBase64LoadRequest request = new DataBase64LoadRequest(userToken);
        @Nullable DataBase64LoadResponse response = domainEndpoint.domainBase64Load(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainBase64SaveTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataBase64SaveRequest request = new DataBase64SaveRequest(userToken);
        @Nullable DataBase64SaveResponse response = domainEndpoint.domainBase64Save(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainBinaryLoadTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataBinaryLoadRequest request = new DataBinaryLoadRequest(userToken);
        @Nullable DataBinaryLoadResponse response = domainEndpoint.domainBinaryLoad(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainBinarySaveTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataBinarySaveRequest request = new DataBinarySaveRequest(userToken);
        @Nullable DataBinarySaveResponse response = domainEndpoint.domainBinarySave(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainJsonLoadFasterXmlTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(userToken);
        @Nullable DataJsonLoadFasterXmlResponse response = domainEndpoint.domainJsonLoadFasterXml(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainJsonLoadJaxBTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataJsonLoadJaxBRequest request = new DataJsonLoadJaxBRequest(userToken);
        @Nullable DataJsonLoadJaxBResponse response = domainEndpoint.domainJsonLoadJaxB(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainJsonSaveFasterXmlTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest(userToken);
        @Nullable DataJsonSaveFasterXmlResponse response = domainEndpoint.domainJsonSaveFasterXml(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainJsonSaveJaxBTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest(userToken);
        @Nullable DataJsonSaveJaxBResponse response = domainEndpoint.domainJsonSaveJaxB(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainXmlLoadFasterXmlTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(userToken);
        @Nullable DataXmlLoadFasterXmlResponse response = domainEndpoint.domainXmlLoadFasterXml(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainXmlLoadJaxBTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(userToken);
        @Nullable DataXmlLoadJaxBResponse response = domainEndpoint.domainXmlLoadJaxB(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainXmlSaveFasterXmlTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(userToken);
        @Nullable DataXmlSaveFasterXmlResponse response = domainEndpoint.domainXmlSaveFasterXml(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainXmlSaveJaxBTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest(userToken);
        @Nullable DataXmlSaveJaxBResponse response = domainEndpoint.domainXmlSaveJaxB(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainYamlLoadFasterXmlTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(userToken);
        @Nullable DataYamlLoadFasterXmlResponse response = domainEndpoint.domainYamlLoadFasterXml(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void domainYamlSaveFasterXmlTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(userToken);
        @Nullable DataYamlSaveFasterXmlResponse response = domainEndpoint.domainYamlSaveFasterXml(request);
        Assert.assertNotNull(response);
    }

}

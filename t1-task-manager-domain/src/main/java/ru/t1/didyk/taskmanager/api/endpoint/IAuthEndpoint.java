package ru.t1.didyk.taskmanager.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.UserLoginRequest;
import ru.t1.didyk.taskmanager.dto.request.UserLogoutRequest;
import ru.t1.didyk.taskmanager.dto.request.UserViewProfileRequest;
import ru.t1.didyk.taskmanager.dto.response.UserLoginResponse;
import ru.t1.didyk.taskmanager.dto.response.UserLogoutResponse;
import ru.t1.didyk.taskmanager.dto.response.UserViewProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserLoginResponse login(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserLoginRequest request);

    @NotNull
    @WebMethod
    UserLogoutResponse logout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserLogoutRequest request);

    @NotNull
    @WebMethod
    UserViewProfileResponse profile(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserViewProfileRequest request);
}

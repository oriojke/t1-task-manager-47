package ru.t1.didyk.taskmanager.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataJsonLoadFasterXmlRequest extends AbstractUserRequest {
    public DataJsonLoadFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}

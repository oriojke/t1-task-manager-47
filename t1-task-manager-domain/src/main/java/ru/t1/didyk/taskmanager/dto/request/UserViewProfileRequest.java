package ru.t1.didyk.taskmanager.dto.request;


import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserViewProfileRequest extends AbstractUserRequest {
    public UserViewProfileRequest(@Nullable String token) {
        super(token);
    }
}

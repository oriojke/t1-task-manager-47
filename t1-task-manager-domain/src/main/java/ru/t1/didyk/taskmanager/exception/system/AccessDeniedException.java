package ru.t1.didyk.taskmanager.exception.system;

public class AccessDeniedException extends AbstractSystemException {

    public AccessDeniedException() {
        super("Error! You are not logged in.");
    }

}

package ru.t1.didyk.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Select("select * from projects where user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<ProjectDTO> findAllWithUserId(@Nullable @Param("userId") String userId);

    @Select("select * from projects")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<ProjectDTO> findAll();

    @Insert("insert into projects (id, name, description, user_id, created, status) " +
            "values (#{id}, #{name}, #{description}, #{userId}, #{created}, #{status})")
    void add(@NotNull ProjectDTO model);

    @Select("select * from projects where id=#{id} and user_id=#{userId} limit 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneByIdWithUserId(@Nullable @Param("userId") String userId, @Param("id") @Nullable String id);

    @Select("select * from projects where id=#{id} limit 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneById(@Param("id") @Nullable String id);

    @Select("select count(*) from projects where user_id=#{userId}")
    int getSize(@Nullable @Param("userId") String userId);

    @Delete("delete from projects where id=#{id}")
    void remove(@Nullable ProjectDTO model);

    @Update("update projects set name=#{name}, description=#{description}, user_id=#{userId}, created=#{created}, status=#{status} " +
            "where id=#{id}")
    void update(final @NotNull ProjectDTO object);
}

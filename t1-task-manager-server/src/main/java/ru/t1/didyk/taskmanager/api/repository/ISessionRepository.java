package ru.t1.didyk.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.model.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @Select("select * from sessions where user_id=#{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<SessionDTO> findAllWithUserId(@Nullable @Param(value = "userId") String userId);

    @Select("select * from sessions")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<SessionDTO> findAll();

    @Insert("insert into sessions (id , user_id, created, role)" +
            "values (#{id}, #{userId}, #{date}, #{role})")
    void add(@NotNull SessionDTO model);

    @Select("select * from sessions where id = #{id} and user_id=#{userId} limit 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable SessionDTO findOneByIdWithUserId(@Nullable @Param(value = "userId") String userId, @Nullable @Param(value = "id") String id);

    @Select("select * from sessions where id = #{id} limit 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable SessionDTO findOneById(@Nullable @Param(value = "id") String id);

    @Select("select count(*) from sessions where user_id=#{userId}")
    int getSize(@Nullable @Param(value = "userId") String userId);

    @Delete("delete from sessions where id = #{id}")
    void remove(@Nullable SessionDTO model);

    @Update("update sessions set user_id=#{userId}, created=#{date}, role=#{role}" +
            "where id=#{id}")
    void update(final @NotNull SessionDTO object);
}

package ru.t1.didyk.taskmanager.api.repository.dto;

import ru.t1.didyk.taskmanager.dto.model.SessionDTO;

public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDTO> {
}

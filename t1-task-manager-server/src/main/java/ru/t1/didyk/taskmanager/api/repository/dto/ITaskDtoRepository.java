package ru.t1.didyk.taskmanager.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    @NotNull
    TaskDTO create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    TaskDTO create(@NotNull String userId, @NotNull String name);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @Nullable String projectId);

}

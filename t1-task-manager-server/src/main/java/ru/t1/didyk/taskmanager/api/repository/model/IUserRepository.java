package ru.t1.didyk.taskmanager.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    Boolean isLoginExists(@NotNull String login);

    @Nullable
    Boolean isEmailExists(@NotNull String email);

}

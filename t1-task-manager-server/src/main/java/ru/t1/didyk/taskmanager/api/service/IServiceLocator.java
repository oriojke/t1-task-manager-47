package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.service.dto.IProjectDtoService;
import ru.t1.didyk.taskmanager.api.service.dto.ITaskDtoService;
import ru.t1.didyk.taskmanager.api.service.dto.IUserDtoService;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskDtoService getTaskService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IUserDtoService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

}

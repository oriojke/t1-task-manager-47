package ru.t1.didyk.taskmanager.api.service.dto;

import ru.t1.didyk.taskmanager.dto.model.SessionDTO;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDTO> {
}

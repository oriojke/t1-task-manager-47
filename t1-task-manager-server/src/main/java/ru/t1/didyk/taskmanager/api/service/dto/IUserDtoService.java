package ru.t1.didyk.taskmanager.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.model.UserDTO;
import ru.t1.didyk.taskmanager.enumerated.Role;

public interface IUserDtoService extends IDtoService<UserDTO> {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO findById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    @Nullable
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @NotNull
    Boolean isLoginExists(@Nullable String login);

    @NotNull
    Boolean isEmailExists(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    @NotNull
    UserDTO removeOne(@Nullable UserDTO model);

}

package ru.t1.didyk.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.endpoint.IAuthEndpoint;
import ru.t1.didyk.taskmanager.api.service.IAuthService;
import ru.t1.didyk.taskmanager.api.service.IServiceLocator;
import ru.t1.didyk.taskmanager.dto.request.UserLoginRequest;
import ru.t1.didyk.taskmanager.dto.request.UserLogoutRequest;
import ru.t1.didyk.taskmanager.dto.request.UserViewProfileRequest;
import ru.t1.didyk.taskmanager.dto.response.UserLoginResponse;
import ru.t1.didyk.taskmanager.dto.response.UserLogoutResponse;
import ru.t1.didyk.taskmanager.dto.response.UserViewProfileResponse;
import ru.t1.didyk.taskmanager.dto.model.SessionDTO;
import ru.t1.didyk.taskmanager.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.didyk.taskmanager.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLoginResponse login(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLoginRequest request) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLogoutResponse logout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLogoutRequest request) {
        final SessionDTO session = check(request);
        getServiceLocator().getAuthService().invalidate(session);
        return new UserLogoutResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public UserViewProfileResponse profile(@WebParam(name = REQUEST, partName = REQUEST) final @NotNull UserViewProfileRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = getServiceLocator().getUserService().findById(userId);
        return new UserViewProfileResponse(user);
    }
}

package ru.t1.didyk.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.repository.model.ISessionRepository;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.api.service.model.ISessionService;
import ru.t1.didyk.taskmanager.model.Session;
import ru.t1.didyk.taskmanager.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull ISessionRepository getRepository(@NotNull EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }
}

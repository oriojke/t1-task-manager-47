package ru.t1.didyk.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @Test
    public void getApplicationVersionTest() {
        Assert.assertNotNull(propertyService.getApplicationVersion());
    }

    @Test
    public void getAuthorEmailTest() {
        Assert.assertNotNull(propertyService.getAuthorEmail());
    }

    @Test
    public void getAuthorNameTest() {
        Assert.assertNotNull(propertyService.getAuthorName());
    }

    @Test
    public void getApplicationConfigTest() {
        Assert.assertNotNull(propertyService.getApplicationConfig());
    }

    @Test
    public void getGitBranchTest() {
        Assert.assertNotNull(propertyService.getGitBranch());
    }

    @Test
    public void getCommitIdTest() {
        Assert.assertNotNull(propertyService.getCommitId());
    }

    @Test
    public void getCommitterNameTest() {
        Assert.assertNotNull(propertyService.getCommitterName());
    }

    @Test
    public void getCommitterEmailTest() {
        Assert.assertNotNull(propertyService.getCommitterEmail());
    }

    @Test
    public void getGitCommitMessageTest() {
        Assert.assertNotNull(propertyService.getGitCommitMessage());
    }

    @Test
    public void getGitCommitTimeTest() {
        Assert.assertNotNull(propertyService.getGitCommitTime());
    }

    @Test
    public void getServerHostTest() {
        Assert.assertNotNull(propertyService.getServerHost());
    }

    @Test
    public void getServerPortTest() {
        Assert.assertNotNull(propertyService.getServerPort());
    }

    @Test
    public void getSessionKeyTest() {
        Assert.assertNotNull(propertyService.getSessionKey());
    }

    @Test
    public void getSessionTimeoutTest() {
        Assert.assertFalse(propertyService.getSessionTimeout() <= 0);
    }

}
